#			Account Policies / Password Policy / 
[System Access]
# Enforce password history
PasswordHistorySize = 24
# Maximum password age
MaximumPasswordAge = 42
# Minimum password age
MinimumPasswordAge = 1
# Minimum password length
MinimumPasswordLength = 7
# Password must meet complexity requirements
PasswordComplexity = 1

# Store passwords using reversible encryption	
Missing Disabled
							
#			Account Policies/Account Lockout Policy
# Account lockout threshold
Missing 	0 invalid logon attempts



#			Local Policies/User Rights Assignment
[Privilege Rights]
#			Local Policies/Security Options
# he4h		/ Accounts 	
# Accounts: Administrator account status
Missing
# Accounts: Rename administrator account
Missing

# he4h		/ Interactive Logon
# Interactive logon: Do not require CTRL+ALT+DEL
Missing		
# Interactive logon: Require Domain Controller authentication to unlock workstation
Missing
	
# he4h		/ Microsoft Network Server	
# Microsoft network server: Disconnect clients when logon hours expire
Missing

# he4h		/ Network Access
# Network access: Allow anonymous SID/Name translation	
Missing
# he4h		/ Network Security
# Network security: Do not store LAN Manager hash value on next password change
Missing
# Network security: Force logoff when logon hours expire
Missing
# he4h		/ Recovery Console
# Recovery console: Allow automatic administrative logon
Missing
# he4h		/ Shutdown
# Shutdown: Allow system to be shut down without having to log on
Missing
# Shutdown: Clear virtual memory pagefile
Missing
# he4h Registry Values
# MACHINE\System\CurrentControlSet\Control\Lsa\RestrictRemoteSAM
Missing


# # # # # User Rights Assignment
[Privilege Rights]
# Local Policies/User Rights Assignment

	#Access this computer from the network
SeNetworkLogonRight = *S-1-5-32-544,*S-1-5-32-555,*S-1-5-80-3847866527-469524349-687026318-516638107-1125189541,*S-1-5-9, STUDENTPC$,DESKTOP-SHUR05P$,Domain Computers

	# Adjust memory quotas for a process
SeIncreaseQuotaPrivilege = *S-1-5-19,*S-1-5-20,*S-1-5-32-544,*S-1-5-6,*S-1-5-80-3847866527-469524349-687026318-516638107-1125189541

	# Allow log on locally
SeInteractiveLogonRight = *S-1-5-32-544,*S-1-5-32-545,*S-1-5-32-548,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551,*S-1-5-80-3847866527-469524349-687026318-516638107-1125189541,*S-1-5-9

	# Bypass traverse checking
SeChangeNotifyPrivilege = *S-1-5-19,*S-1-5-20,*S-1-5-32-544,*S-1-5-6,*S-1-5-80-3847866527-469524349-687026318-516638107-1125189541

	# Change the system time
SeSystemtimePrivilege = *S-1-5-19,*S-1-5-20,*S-1-5-32-544,*S-1-5-6,*S-1-5-80-3847866527-469524349-687026318-516638107-1125189541

	# Create symbolic links
SeCreateSymbolicLinkPrivilege = *S-1-5-32-544,*S-1-5-80-3847866527-469524349-687026318-516638107-1125189541

	# Generate security audits
SeAuditPrivilege = *S-1-5-19,*S-1-5-20,*S-1-5-32-544,*S-1-5-6,*S-1-5-80-3847866527-469524349-687026318-516638107-1125189541

	# Replace a process level token
SeAssignPrimaryTokenPrivilege = *S-1-5-19,*S-1-5-20,*S-1-5-6,*S-1-5-80-3847866527-469524349-687026318-516638107-1125189541

	# Take ownership of files or other objects
SeTakeOwnershipPrivilege = *S-1-5-19,*S-1-5-20,*S-1-5-32-544,*S-1-5-6,*S-1-5-80-3847866527-469524349-687026318-516638107-1125189541




# Not Defined
	# Act as part of the operating system
SeTcbPrivilege = *S-1-5-21-1968834540-465677593-1751497648-1117

# Not Defined
	# Add workstations to domain
SeMachineAccountPrivilege = *S-1-5-11

# Not Defined
	# Back up files and directories
SeBackupPrivilege = *S-1-5-32-544,*S-1-5-32-549,*S-1-5-32-551

# Not Defined
	# Create a pagefile
SeCreatePagefilePrivilege = *S-1-5-32-544

# Not Defined
	# Create permanent shared objects
SeCreatePermanentPrivilege = *S-1-5-80-3847866527-469524349-687026318-516638107-1125189541

# Not Defined
	# Create global objects
SeCreateGlobalPrivilege = *S-1-5-19,*S-1-5-20,*S-1-5-80-3847866527-469524349-687026318-516638107-1125189541

# Not Defined
	# Create a token object
SeCreateTokenPrivilege = *S-1-5-21-1968834540-465677593-1751497648-1117

# Not Defined
	# Debug programs
SeDebugPrivilege = *S-1-5-32-544

# Not Defined
	# Force shutdown from a remote system
SeRemoteShutdownPrivilege = *S-1-5-32-544,*S-1-5-32-549

# Not Defined
	# Increase scheduling priority
SeIncreaseBasePriorityPrivilege = *S-1-5-32-544

# Not Defined
	# Load and unload device drivers
SeLoadDriverPrivilege = *S-1-5-32-544,*S-1-5-32-550

# Not Defined
	# Log on as a batch job
SeBatchLogonRight = *S-1-5-32-544,*S-1-5-32-551,*S-1-5-32-559

# Not Defined
	# Log on as a service
SeServiceLogonRight = *S-1-5-21-1968834540-465677593-1751497648-1117

# Not Defined
	# Manage auditing and security log
SeSecurityPrivilege = *S-1-5-32-544

# Not Defined
	# Modify firmware environment values
SeSystemEnvironmentPrivilege = *S-1-5-32-544

# Not Defined
	# Profile single process
SeProfileSingleProcessPrivilege = *S-1-5-32-544

# Not Defined
	# Profile system performance
SeSystemProfilePrivilege = *S-1-5-32-544,*S-1-5-80-3139157870-2983391045-3678747466-658725712-1809340420

# Not Defined
	# Restore files and directories
SeRestorePrivilege = *S-1-5-32-544,*S-1-5-32-549,*S-1-5-32-551

# Not Defined
	# Shut down the system
SeShutdownPrivilege = *S-1-5-32-544,*S-1-5-32-549,*S-1-5-32-550,*S-1-5-32-551

# Not Defined 
	# Remove computer from docking station
SeUndockPrivilege = *S-1-5-32-544

# Not Defined 
	# Enable computer and user accounts to be trusted for delegation
SeEnableDelegationPrivilege = *S-1-5-32-544

# Not Defined 
	# Perform volume maintenance tasks
SeManageVolumePrivilege = *S-1-5-32-544

# Not Defined 
	# Allow log on through Remote Desktop Services
SeRemoteInteractiveLogonRight = *S-1-5-21-1968834540-465677593-1751497648-1117

# Not Defined
	# Impersonate a client after authentication
SeImpersonatePrivilege = *S-1-5-19,*S-1-5-20,*S-1-5-21-1968834540-465677593-1751497648-1117,*S-1-5-32-544,*S-1-5-6

# Not Defined
	# Increase a process working set
SeIncreaseWorkingSetPrivilege = *S-1-5-32-545

# Not Defined
	#Change the time zone
SeTimeZonePrivilege = *S-1-5-19,*S-1-5-32-544,*S-1-5-32-549

# Prolly Not Defined
	#Obtain an impersonation token for another user in the same session
	# Breaks shit. Careful
SeDelegateSessionUserImpersonatePrivilege = *S-1-5-21-1968834540-465677593-1751497648-1117











# Account Policies/Kerberos Policy
[Kerberos Policy]
TicketValidateClient = 1
MaxServiceAge = 600
MaxTicketAge = 10
MaxRenewAge = 7
MaxClockSkew = 5


[Registry Values]
MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Setup\RecoveryConsole\SecurityLevel=4,1
MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Setup\RecoveryConsole\SetCommand=4,0

MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\CachedLogonsCount=1,"10"
MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\ForceUnlockLogon=4,1
MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\PasswordExpiryWarning=4,5
MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\ScRemoveOption=1,"0"

MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\ConsentPromptBehaviorAdmin=4,5
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\ConsentPromptBehaviorUser=4,3
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\DisableCAD=4,1
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\DontDisplayLastUserName=4,0
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\EnableInstallerDetection=4,1
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\EnableLUA=4,1
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\EnableSecureUIAPaths=4,1
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\EnableUIADesktopToggle=4,0
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\EnableVirtualization=4,1
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\FilterAdministratorToken=4,0
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\LegalNoticeCaption=1,""
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\LegalNoticeText=7,
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\PromptOnSecureDesktop=4,1
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\ScForceOption=4,0
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\ShutdownWithoutLogon=4,1
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\UndockWithoutLogon=4,1
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\ValidateAdminCodeSignatures=4,0

MACHINE\Software\Policies\Microsoft\Windows\Safer\CodeIdentifiers\AuthenticodeEnabled=4,0

MACHINE\System\CurrentControlSet\Control\Lsa\AuditBaseObjects=4,0
MACHINE\System\CurrentControlSet\Control\Lsa\CrashOnAuditFail=4,0
MACHINE\System\CurrentControlSet\Control\Lsa\DisableDomainCreds=4,0
MACHINE\System\CurrentControlSet\Control\Lsa\EveryoneIncludesAnonymous=4,0
MACHINE\System\CurrentControlSet\Control\Lsa\FIPSAlgorithmPolicy\Enabled=4,0
MACHINE\System\CurrentControlSet\Control\Lsa\ForceGuest=4,0
MACHINE\System\CurrentControlSet\Control\Lsa\FullPrivilegeAuditing=3,0
MACHINE\System\CurrentControlSet\Control\Lsa\LimitBlankPasswordUse=4,1
MACHINE\System\CurrentControlSet\Control\Lsa\MSV1_0\NTLMMinClientSec=4,536870912
MACHINE\System\CurrentControlSet\Control\Lsa\MSV1_0\NTLMMinServerSec=4,536870912
MACHINE\System\CurrentControlSet\Control\Lsa\NoLMHash=4,1
MACHINE\System\CurrentControlSet\Control\Lsa\RestrictAnonymous=4,0
MACHINE\System\CurrentControlSet\Control\Lsa\RestrictAnonymousSAM=4,1
MACHINE\System\CurrentControlSet\Control\Lsa\RestrictRemoteSAM=1,"O:BAG:BAD:(A;;RC;;;AA)(A;;RC;;;BA)(A;;RC;;;S-1-5-21-1968834540-465677593-1751497648-1116)(A;;RC;;;DC)(A;;RC;;;S-1-5-19)(A;;RC;;;S-1-5-21-1968834540-465677593-1751497648-1115)(A;;RC;;;SY)"

MACHINE\System\CurrentControlSet\Control\Print\Providers\LanMan Print Services\Servers\AddPrinterDrivers=4,1
MACHINE\System\CurrentControlSet\Control\SecurePipeServers\Winreg\AllowedExactPaths\Machine=7,System\CurrentControlSet\Control\ProductOptions,System\CurrentControlSet\Control\Server Applications,Software\Microsoft\Windows NT\CurrentVersion
MACHINE\System\CurrentControlSet\Control\SecurePipeServers\Winreg\AllowedPaths\Machine=7,System\CurrentControlSet\Control\Print\Printers,System\CurrentControlSet\Services\Eventlog,Software\Microsoft\OLAP Server,Software\Microsoft\Windows NT\CurrentVersion\Print,Software\Microsoft\Windows NT\CurrentVersion\Windows,System\CurrentControlSet\Control\ContentIndex,System\CurrentControlSet\Control\Terminal Server,System\CurrentControlSet\Control\Terminal Server\UserConfig,System\CurrentControlSet\Control\Terminal Server\DefaultUserConfiguration,Software\Microsoft\Windows NT\CurrentVersion\Perflib,System\CurrentControlSet\Services\SysmonLog

MACHINE\System\CurrentControlSet\Control\Session Manager\Kernel\ObCaseInsensitive=4,1
MACHINE\System\CurrentControlSet\Control\Session Manager\Memory Management\ClearPageFileAtShutdown=4,0
MACHINE\System\CurrentControlSet\Control\Session Manager\ProtectionMode=4,1
MACHINE\System\CurrentControlSet\Control\Session Manager\SubSystems\optional=7,

MACHINE\System\CurrentControlSet\Services\LanManServer\Parameters\AutoDisconnect=4,15
MACHINE\System\CurrentControlSet\Services\LanManServer\Parameters\EnableForcedLogOff=4,0
MACHINE\System\CurrentControlSet\Services\LanManServer\Parameters\EnableSecuritySignature=4,1
MACHINE\System\CurrentControlSet\Services\LanManServer\Parameters\NullSessionPipes=7,,netlogon,samr,lsarpc
MACHINE\System\CurrentControlSet\Services\LanManServer\Parameters\RequireSecuritySignature=4,1
MACHINE\System\CurrentControlSet\Services\LanManServer\Parameters\RestrictNullSessAccess=4,1

MACHINE\System\CurrentControlSet\Services\LanmanWorkstation\Parameters\EnablePlainTextPassword=4,0
MACHINE\System\CurrentControlSet\Services\LanmanWorkstation\Parameters\EnableSecuritySignature=4,1
MACHINE\System\CurrentControlSet\Services\LanmanWorkstation\Parameters\RequireSecuritySignature=4,0

MACHINE\System\CurrentControlSet\Services\LDAP\LDAPClientIntegrity=4,1

MACHINE\System\CurrentControlSet\Services\Netlogon\Parameters\DisablePasswordChange=4,0
MACHINE\System\CurrentControlSet\Services\Netlogon\Parameters\MaximumPasswordAge=4,30
MACHINE\System\CurrentControlSet\Services\Netlogon\Parameters\RequireSignOrSeal=4,1
MACHINE\System\CurrentControlSet\Services\Netlogon\Parameters\RequireStrongKey=4,1
MACHINE\System\CurrentControlSet\Services\Netlogon\Parameters\SealSecureChannel=4,1
MACHINE\System\CurrentControlSet\Services\Netlogon\Parameters\SignSecureChannel=4,1
MACHINE\System\CurrentControlSet\Services\NTDS\Parameters\LDAPServerIntegrity=4,1


# If the value for “Interactive logon: Require domain controller authentication to unlock workstation” is not set to “Disabled”, then this is a finding. 
MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\ForceUnlockLogon=4,0
# If the value for “Recovery Console: Allow automatic administrative logon” is not set to “Disabled”, then this is a finding. 
MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Setup\RecoveryConsole\SecurityLevel=4,1
# Title	Definition Id	Comment CTRL+ALT+DEL Not Required for Logon
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\DisableCAD=4,1
# System Shut Down Not Allowed Without Having to Log On
MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System\ShutdownWithoutLogon=4,1
# Microsoft network server: Disconnect clients when logon hours expire
MACHINE\System\CurrentControlSet\Services\LanManServer\Parameters\EnableForcedLogOff=4,0


[Event Audit]

AuditSystemEvents = 0
AuditLogonEvents = 0
AuditObjectAccess = 0
AuditPrivilegeUse = 0
AuditPolicyChange = 0
AuditAccountManage = 0
AuditProcessTracking = 0
AuditDSAccess = 0
AuditAccountLogon = 0
