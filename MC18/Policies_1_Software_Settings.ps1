# he3			/ #			Account Policies / Password Policy / 
	# he4i
		#		/ Enforce password history
				PasswordHistorySize = 24
		#		/ Maximum password age
				MaximumPasswordAge = 42
		#		/ Minimum password age
				MinimumPasswordAge = 1
		#		/ Minimum password length
				MinimumPasswordLength = 7
		#		/ Password must meet complexity requirements
				PasswordComplexity = 1
		#		/ Store passwords using reversible encryption	
				Missing Disabled
							
# he3			/ #			Account Policies/Account Lockout Policy
	# he4i
		#		/ Account lockout threshold
				Missing 	0 invalid logon attempts
	
# he3			/ #			Local Policies/User Rights Assignment
	# he4i
		#		/ Access this computer from the network
				Missing Administrators, ENTERPRISE DOMAIN CONTROLLERS,
		#		/ Adjust memory quotas for a process
			Missing
		#		/ Allow log on locally
			Missing
		#		/ Bypass traverse checking
		
		#		/ Change the system time

		#		/ Create symbolic links

		#		/ Generate security audits

		#		/ Replace a process level token

		#		/ Take ownership of files or other objects
			Missing
			
# he3			/ #			Local Policies/Security Options
	# he4h		/ Accounts
		# he4i	
			# 	/ Accounts: Administrator account status
			
			#	/ Accounts: Rename administrator account
				Missing
	
	# he4h		/ Interactive Logon
		# he4i	
			#	/ Interactive logon: Do not require CTRL+ALT+DEL
			
			#	/ Interactive logon: Require Domain Controller authentication to unlock workstation
				Missing
	
	# he4h		/ Microsoft Network Server
		# he4i	
			#	/ Microsoft network server: Disconnect clients when logon hours expire
				Missing

	# he4h		/ Network Access
		# he4i	
			#	/ Network access: Allow anonymous SID/Name translation	
				Missing
	# he4h		/ Network Security
		# he4i	
			#	/ Network security: Do not store LAN Manager hash value on next password change
				Missing
			#	/ Network security: Force logoff when logon hours expire
				Missing
	# he4h		/ Recovery Console
		# he4i	
			#	/ Recovery console: Allow automatic administrative logon
				Missing
	# he4h		/ Shutdown
		# he4i
			#	/ Shutdown: Allow system to be shut down without having to log on
				Missing
			#	/ Shutdown: Clear virtual memory pagefile
				Missing
	# he4h		/ Registry Values
		# he4i
			#	/ MACHINE\System\CurrentControlSet\Control\Lsa\RestrictRemoteSAM
				Missing
