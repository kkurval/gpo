# he1					/ ###		Administrative Templates

#he4i		/ Policy definitions (ADMX files) retrieved from the local computer
# he3			/ #			Network/Network Connections/Windows Firewall/Domain Profile
# he4i
# Windows Firewall: Allow ICMP exceptions
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile\IcmpSettings" -ValueName "AllowOutboundDestinationUnreachable" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile\IcmpSettings" -ValueName "AllowOutboundSourceQuench" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile\IcmpSettings" -ValueName "AllowRedirect" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile\IcmpSettings" -ValueName "AllowInboundEchoRequest" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile\IcmpSettings" -ValueName "AllowOutboundTimeExceeded" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile\IcmpSettings" -ValueName "AllowOutboundParameterProblem" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile\IcmpSettings" -ValueName "AllowInboundTimestampRequest" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile\IcmpSettings" -ValueName "AllowInboundMaskRequest" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile\IcmpSettings" -ValueName "AllowOutboundPacketTooBig" -Type DWORD -Value 1

# Windows Firewall: Allow inbound remote administration exception

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\RemoteAdminSettings" -ValueName "Enabled" -Type DWORD -Value 1
# SZ:192.168.0.254
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\RemoteAdminSettings" -ValueName "RemoteAddresses" -Type ExpandString -Value 

# Windows Firewall: Allow inbound Remote Desktop exceptions

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\Services\RemoteDesktop" -ValueName "Enabled" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\Services\RemoteDesktop" -ValueName "RemoteAddresses" -Type ExpandString -Value "192.168.0.254"

# Windows Firewall: Allow local port exceptions

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts" -ValueName "Enabled" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts" -ValueName "AllowUserPrefMerge" -Type DWORD -Value 1

# Windows Firewall: Allow local program exceptions

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\AuthorizedApplications" -ValueName "AllowUserPrefMerge" -Type DWORD -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\AuthorizedApplications" -ValueName "Enabled" -Type DWORD -Value 1

# Windows Firewall: Define inbound port exceptions

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "22:TCP:192.168.0.0/24:enabled:SSH" -Type ExpandString -Value "22:TCP:192.168.0.0/24:enabled:SSH"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "22:UDP:192.168.0.0/24:enabled:SSH" -Type ExpandString -Value "22:UDP:192.168.0.0/24:enabled:SSH"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "42:TCP:192.168.0.0/24:enabled:WINS replication" -Type ExpandString -Value "42:TCP:192.168.0.0/24:enabled:WINS replication"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "42:UDP:192.168.0.0/24:enabled:WINS replication" -Type ExpandString -Value "42:UDP:192.168.0.0/24:enabled:WINS replication"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "53:TCP:192.168.0.0/24:enabled:DNS" -Type ExpandString -Value "53:TCP:192.168.0.0/24:enabled:DNS"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "53:UDP:192.168.0.0/24:enabled:DNS" -Type ExpandString -Value "53:UDP:192.168.0.0/24:enabled:DNS"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "67:UDP:192.168.0.0/24:enabled:DHCP" -Type ExpandString -Value "67:UDP:192.168.0.0/24:enabled:DHCP"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "88:TCP:192.168.0.0/24:enabled:Kerberos" -Type ExpandString -Value "88:TCP:192.168.0.0/24:enabled:Kerberos"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "88:UDP:192.168.0.0/24:enabled:Kerberos" -Type ExpandString -Value "88:UDP:192.168.0.0/24:enabled:Kerberos"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "123:UDP:192.168.0.0/24:enabled:Windows Time" -Type ExpandString -Value "123:UDP:192.168.0.0/24:enabled:Windows Time"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "135:TCP:192.168.0.0/24:enabled:RPC" -Type ExpandString -Value "135:TCP:192.168.0.0/24:enabled:RPC"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "135:UDP:192.168.0.0/24:enabled:RPC" -Type ExpandString -Value "135:UDP:192.168.0.0/24:enabled:RPC"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "137:TCP:192.168.0.0/24:enabled:NetBios name service" -Type ExpandString -Value "137:TCP:192.168.0.0/24:enabled:NetBios name service"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "137:UDP:192.168.0.0/24:enabled:NetBios name service" -Type ExpandString -Value "137:UDP:192.168.0.0/24:enabled:NetBios name service"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "137:UDP:192.168.0.0/24:enabled:NetBios name service" -Type ExpandString -Value "137:UDP:192.168.0.0/24:enabled:NetBios name service"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "138:UDP:192.168.0.0/24:enabled:NetBios datagram service" -Type ExpandString -Value "138:UDP:192.168.0.0/24:enabled:NetBios datagram service"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "139:UDP:192.168.0.0/24:enabled:NetBios session service" -Type ExpandString -Value "139:UDP:192.168.0.0/24:enabled:NetBios session service"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "389:TCP:192.168.0.0/24:enabled:LDAP" -Type ExpandString -Value "389:TCP:192.168.0.0/24:enabled:LDAP"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "389:UDP:192.168.0.0/24:enabled:LDAP" -Type ExpandString -Value "389:UDP:192.168.0.0/24:enabled:LDAP"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "445:TCP:192.168.0.0/24:enabled:SMB" -Type ExpandString -Value "445:TCP:192.168.0.0/24:enabled:SMB"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "445:UDP:192.168.0.0/24:enabled:SMB" -Type ExpandString -Value "445:UDP:192.168.0.0/24:enabled:SMB"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "464:TCP:192.168.0.0/24:enabled:Kerberos PC" -Type ExpandString -Value "464:TCP:192.168.0.0/24:enabled:Kerberos PC"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "464:UDP:192.168.0.0/24:enabled:Kerberos PC" -Type ExpandString -Value "464:UDP:192.168.0.0/24:enabled:Kerberos PC"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "636:TCP:192.168.0.0/24:enabled:LDAP over SSL" -Type ExpandString -Value "636:TCP:192.168.0.0/24:enabled:LDAP over SSL"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "1512:TCP:192.168.0.0/24:enabled:WINS" -Type ExpandString -Value "1512:TCP:192.168.0.0/24:enabled:WINS"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "1512:UDP:192.168.0.0/24:enabled:WINS" -Type ExpandString -Value "1512:UDP:192.168.0.0/24:enabled:WINS"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "2535:UDP:192.168.0.0/24:enabled:DHCP" -Type ExpandString -Value "2535:UDP:192.168.0.0/24:enabled:DHCP"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "3268:TCP:192.168.0.0/24:enabled:Global catalog LDAP" -Type ExpandString -Value "3268:TCP:192.168.0.0/24:enabled:Global catalog LDAP"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "5722:TCP:192.168.0.0/24:enabled:File Replication RPC, DFSR (SYSVOL)" -Type ExpandString -Value "5722:TCP:192.168.0.0/24:enabled:File Replication RPC, DFSR (SYSVOL)"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "1025-5000:TCP:192.168.0.0/24:enabled:RPC dynamic assignment" -Type ExpandString -Value "1025-5000:TCP:192.168.0.0/24:enabled:RPC dynamic assignment"

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "49152-65535:TCP:192.168.0.0/24:enabled:RPC dynamic assignment" -Type ExpandString -Value "49152-65535:TCP:192.168.0.0/24:enabled:RPC dynamic assignment"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\GloballyOpenPorts\List" -ValueName "49152-65535:UDP:192.168.0.0/24:enabled:RPC dynamic assignment" -Type ExpandString -Value "49152-65535:UDP:192.168.0.0/24:enabled:RPC dynamic assignment"

# Windows Firewall: Define inbound program exceptions

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\AuthorizedApplications\List" -ValueName "C:\\Program Files\\OpenSSH\\sshd.exe:0.0.0.0/0:enabled:SSHD" -Type ExpandString -Value "C:\\Program Files\\OpenSSH\\sshd.exe:0.0.0.0/0:enabled:SSHD"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\AuthorizedApplications\List" -ValueName "C:\\Emuldata\\USER_EMULATION.exe:0.0.0.0/0:enabled:user_eumlation_1" -Type ExpandString -Value "C:\\Emuldata\\USER_EMULATION.exe:0.0.0.0/0:enabled:user_eumlation_1"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile\AuthorizedApplications\List" -ValueName "C:\\Emuldata\\USER_EMULATION_2.exe:0.0.0.0/0:enabled:user_eumlation_3" -Type ExpandString -Value "C:\\Emuldata\\USER_EMULATION_2.exe:0.0.0.0/0:enabled:user_eumlation_3"

# Windows Firewall: Do not allow exceptions

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile" -ValueName "DoNotAllowExceptions" -Type DWORD -Value 0

# he3			/ #			Network/Network Connectivity Status Indicator
# he4i

# Specify corporate DNS probe host address

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\NetworkConnectivityStatusIndicator\CorporateConnectivity" -ValueName "DnsProbeContent" -Type ExpandString -Value $DNS_Server_IP

# Specify corporate DNS probe host name

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\NetworkConnectivityStatusIndicator\CorporateConnectivity" -ValueName "DnsProbeHost" -Type ExpandString -Value $DNS_Server_Host

# Specify domain location determination URL

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\NetworkConnectivityStatusIndicator\CorporateConnectivity" -ValueName "DomainLocationDeterminationUrl" -Type ExpandString -Value $Domain_Location_URL

# he3			/ #			Network/Windows Connection Manager
# he4i
# Disable power management in connected standby mode

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WcmSvc\GroupPolicy" -ValueName "fDisablePowerManagement" -Type DWORD -Value 1

# Minimize the number of simultaneous connections to the Internet or a Windows Domain

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WcmSvc\GroupPolicy" -ValueName "fMinimizeConnections" -Type DWORD -Value 0

# Prohibit connection to non-domain networks when connected to domain authenticated network

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WcmSvc\GroupPolicy" -ValueName "fBlockNonDomain" -Type DWORD -Value 0

# Prohibit connection to roaming Mobile Broadband networks

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WcmSvc\GroupPolicy" -ValueName "fBlockRoaming" -Type DWORD -Value 1

# he3			/ #			System/Windows Time Service/Time Providers
# he4i
# Configure Windows NTP Client

# NtpServer
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\W32Time\Parameters" -ValueName "NtpServer" -Type ExpandString -Value "time.windows.com,0x9"

# Type
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\W32Time\Parameters" -ValueName "NtpServer" -Type ExpandString -Value "NT5DS"

# CrossSiteSyncFlags
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\W32Time\TimeProviders\NtpClient" -ValueName "CrossSiteSyncFlags" -Type DWORD -Value 2

# ResolvePeerBackoffMinutes
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\W32Time\TimeProviders\NtpClient" -ValueName "ResolvePeerBackoffMinutes" -Type DWORD -Value 15

# ResolvePeerBackoffMaxTimes
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\W32Time\TimeProviders\NtpClient" -ValueName "ResolvePeerBackoffMaxTimes" -Type DWORD -Value 7

# SpecialPollInterval
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\W32Time\TimeProviders\NtpClient" -ValueName "SpecialPollInterval" -Type DWORD -Value 3600

# EventLogFlags
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\W32Time\TimeProviders\NtpClient" -ValueName "EventLogFlags" -Type DWORD -Value 0

# Enable Windows NTP Client
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\W32Time\TimeProviders\NtpClient" -ValueName "EventLogFlags" -Type DWORD -Value 1

# Enable Windows NTP Server
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\W32Time\TimeProviders\NtpServer" -ValueName "Enabled" -Type DWORD -Value 1

# he3			/ #			Windows Components/Windows Update
# he4i
# Configure Automatic Updates

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -ValueName "AutomaticMaintenanceEnabled" DELETE
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -ValueName "NoAutoUpdate" -Type DWORD -Value 0
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -ValueName "AUOptions" -Type DWORD -Value 2
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -ValueName "AllowMUUpdateService" DELETE
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -ValueName "ScheduledInstallDay" -Type DWORD -Value 7
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -ValueName "ScheduledInstallTime" -Type DWORD -Value 0


# No auto-restart with logged on users for scheduled automatic updates installations

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -ValueName "NoAutoRebootWithLoggedOnUsers" -Type DWORD -Value 1

# he3			/ #			Extra Registry Settings
# Display names for some settings cannot be found. You might be able to resolve this issue by updating the .ADM files used by Group Policy Management.
# This configuration is usually under Computer Configuration:Preferences:Windows Settings:Registry
# But with Set-GPRegistryValue it gets set under Computer Configuration:Policies:Administrative Templates:Registry Extra settings
# he4i
# Windows logon 
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -ValueName "AutoAdminLogon" -Type ExpandString -Value 1
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -ValueName "DefaultDomainName" -Type ExpandString -Value $NetBIOS
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -ValueName "DefaultPassword" -Type ExpandString -Value $Password
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -ValueName "DefaultUserName" -Type ExpandString -Value $UserName
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -ValueName "EnableFirstLogonAnimation" -Type DWORD -Value "00000000"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -ValueName "ForceUnlockLogon" -Type DWORD -Value "00000001"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -ValueName "ShutdownWithoutLogon" -Type ExpandString -Value "1"
