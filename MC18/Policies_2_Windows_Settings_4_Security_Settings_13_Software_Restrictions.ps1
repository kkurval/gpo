#			Software Restriction Policies			# EXPORTED FROM registry.pol
#		 Enforcement			
# Apply Software Restriction Policies to the following
All software files except libraries (such as DLLs)
# Apply Software Restriction Policies to the following users
All users
# When applying Software Restriction Policies
Ignore certificate rules
# Designated File Types
# ADE
ADE File
#		Trusted Publishers
# Trusted publisher management
Allow all administrators and users to manage users own Trusted Publishers
# Certificate verification
None

#			Software Restriction Policies/Security Levels
# Default Security Level 	Unrestricted
Unrestricted

#			Software Restriction Policies/Additional Rules
#		Certificate Rules
#		ROOTCA-Northwindtraders.com 
# Security Level
# Description
# Date last modified
# Winning GPO								