# GPO variables
$Source_GPO_Name            = 'Blank Template GPO'
$Target_GPO_Name            = 'Research4u GPO'
$GPO_Name                   = $Target_GPO_Name
$NetBIOS                    = "research4u"


$Domain                     = 'research4u.co.uk'
$Base_Template_ZIP          = 'https://bitbucket.org/kkurval/gpo/raw/master/Blank_Template_GPO_Machine.zip'
$Base_Template              = 'Base_Template'

$Access_Rights_Account_1    = "LABSettings"
$Display_Name_Account_1     = "Lab Settings"
$Description_Account_1      = "DO NOT MODIFY OR REMOVE. This group is only meant for modifing lab settings."

$Random_GUID

$DNS_Server_IP              = '192.168.6.254'
$DNS_Server_Host            = 'dc1.research4u.co.uk'
$TIMESERVER                 = '192.168.6.254'
$Domain_Location_URL        = '192.168.6.??'


#$Direction = 'In' # 'Out'
#$Protocol  = '6'  # 6 -> TCP, 17 -> UDP

# $host = 'john'
# $inbound_ports = @(22, 137, 138, 139)
# $outbound_ports = @(22, 137, 138, 139)

# $host = 'jane'
# $inbound_ports = @(22, 137, 138, 139)
# $outbound_ports = @(22, 137, 138, 139)

# $host = 'dc1'
# $inbound_ports = @(22, 137, 138, 139)
# $outbound_ports = @(22, 137, 138, 139)

# $host = 'dc2'
# $inbound_ports = @(22, 137, 138, 139)
# $outbound_ports = @(22, 137, 138, 139)


# Create OU_Path from domain name
$d = $Domain.split('.') ; Foreach ($i in $d) { $f = $f + "DC=" + $i + ","} ; $OU_Path = $f.Substring(0,$f.Length-1)


# Download and extract Blank GPO
(New-Object System.Net.WebClient).DownloadFile($Base_Template_ZIP, $Base_Template + '.zip') ; Unblock-File -Path '.\' + $Base_Template + '.zip'
# CONFIRM THAT DOWNLOAD WAS SUCCESSFUL. Compare file hash.
Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip {param([string]$zipfile, [string]$outpath), [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)}
Unzip ".\"+$Base_Template+".zip" ".\"


# Security Settings Access Rights !!!
$SR = @()
    # Local Policies/User Rights Assignment [Privilege Rights]
#$Add_Rights = Get_SID(STUDENTPC$,DESKTOP-SHUR05P$,Domain Computers)
$SR = $SR + 87,
$SR = $SR + 90,93,96,99,102,105,108,111

    # !!
# Certificate

# Administrative Templates
$FW = @()
    # Default outbound ports
$FW = $FW + 40..52
    # Default inbound ports
$FW = $FW + 56..60


$FW |  foreach {
    # Get registry item
    $string = (Get-Content .\Policies_2_Windows_Settings_4_Security_Settings_9_Firewall.ps1)[$_].Trim()
    $NewString = ''
    # Append in current variables
    $string.trim().split(' ') | foreach {
            if ( $_.StartsWith("$")) { $y = (Get-Variable -Name $_.Substring(1)).Value }
            else { $y = $_ }
            $NewString = $NewString + $y + " "
            }
    # Execute item in powershell
    $NewString  # | powershell
    }

# Administrative Templates
$AT = @()
    # Windows Firewall: Allow ICMP exceptions
$AT = $AT + 7..15
    # Windows Firewall: Allow inbound remote administration exception
$AT = $AT + 19, 21
    # Windows Firewall: Allow inbound Remote Desktop exceptions
$AT = $AT + 25, 26
    # Windows Firewall: Allow local port exceptions
$AT = $AT + 30, 31
    # Windows Firewall: Allow local program exceptions
$AT = $AT + 35, 36
    # Windows Firewall: Do not allow exceptions

# SKIPPING FIREWALL STUFF UPTO 330
# New Rules should be under dynamic reg location. Or Security settings location for better visibility
# Probaby both


$AT = $AT + 82
    # Specify corporate DNS probe host address           $DNS_Server_IP
$AT = $AT + 89
    # Specify corporate DNS probe host name              $DNS_Server_Host
$AT = $AT + 93
    # Specify domain location determination URL          $Domain_Location_URL
$AT = $AT + 97
    # NtpServer                                          $TIMESERVER?
$AT = $AT + 122
    # Type
$AT = $AT + 125
    # CrossSiteSyncFlags
$AT = $AT + 128
    # Enable Windows NTP Client
$AT = $AT + 122
    # Enable Windows NTP Server
$AT = $AT + 146
    # Configure Automatic Updates
$AT = $AT + 152..157
    # No auto-restart with logged on users for scheduled automatic updates installations
$AT = $AT + 162
    # Windows logon                                     $NetBios $Password $Username
    # Probably remove.  USE DYNAMIC WMI query based rule instead
$AT = $AT + 170..176


$AT |  foreach {
    # Get registry item
    $string = (Get-Content .\Policies_3_Administrative_Template.ps1)[$_].Trim()
    $NewString = ''
    # Append in current variables
    $string.trim().split(' ') | foreach {
            if ( $_.StartsWith("$")) { $y = (Get-Variable -Name $_.Substring(1)).Value }
            else { $y = $_ }
            $NewString = $NewString + $y + " "
            }
    # Execute item in powershell
    $NewString  # | powershell
    }

# SEPARATE RULE FOR FIREWALL RULES AND OTHER REPEATED GPO SETTINGS
#
# WINDOWS SETTINGS MODULAR REGISTRY KEYS
# Preferences_2_Control_Panel_Settings_3_Folder_Options.ps1
$WR = @()
$Password                   = ''
$Username                   = 'john'
$WMI_Query                  = "SELECT SMBIOSBIOSVersion FROM Win32_BIOS where SMBIOSBIOSVersion Like 'mc18-john-r4u-%'"
    # Auto login user
$WR = $WR + 1..8
# WRITE TO REGISTRY.XML files

$Password                   = ''
$Username                   = 'jane'
$WMI_Query                  = "SELECT SMBIOSBIOSVersion FROM Win32_BIOS where SMBIOSBIOSVersion Like 'mc18-jane-r4u-%'"
    # Auto login user
$WR = $WR + 1..8
# WRITE TO REGISTRY.XML files

    # Host specific firewall rules

$WMI_Query                  = "SELECT SMBIOSBIOSVersion FROM Win32_BIOS where SMBIOSBIOSVersion Like 'mc18-dc1-r4u-%'"
    # Auto login user
# $WR = $WR +
# WRITE TO REGISTRY.XML files


# Import GPO from zip folder
Import-GPO -BackupGpoName $Source_GPO_Name -TargetName $Target_GPO_Name  -path ".\"+$Base_Template -CreateIfNeeded

# Link GPO to domain
New-GPLink -Name $Source_GPO_Name -Target $OU_Path -Enforced Yes -LinkEnabled Yes -Order 1

# Create Groups
New-ADGroup -Name $Display_Name_Account_1 -SamAccountName $Access_Rights_Account_1 -GroupCategory Security -GroupScope Domainlocal -DisplayName $Display_Name_Account_1 -Description $Description_Account_1

# Create Users
    #

# CREATE ORGANIZATIONAL UNITS
# Research4u.co.uk Domain OU
New-ADOrganizationalUnit -Name "Research4u" -Path $OU_Path -ProtectedFromAccidentalDeletion $True
# Computers OU
New-ADOrganizationalUnit -Name "Computers" -Path "OU=Research4u, " + $OU_Path -ProtectedFromAccidentalDeletion $True
New-ADOrganizationalUnit -Name "Domain Controllers" -Path " OU=Computers, " + $OU_Path -ProtectedFromAccidentalDeletion $True
New-ADOrganizationalUnit -Name "Workstations" -Path "OU=Computers, OU=Research4u, " + $OU_Path -ProtectedFromAccidentalDeletion $True
# New-ADOrganizationalUnit -Name "Windows_8" -Path "OU=Workstations, OU=Computers, OU=Research4u, " + $OU_Path -ProtectedFromAccidentalDeletion $True
# New-ADOrganizationalUnit -Name "Windows_10" -Path "OU=Workstations, OU=Computers, OU=Research4u, " + $OU_Path -ProtectedFromAccidentalDeletion $True

# Move domain objects to organizational units
# Windows 2016 # NORTH
#Move-ADObject -Identity 'c718a3f6-e77a-432f-a0a1-991595591f92' -TargetPath "OU=Domain Controllers, OU=Computers, OU=Research4u, DC=research4u,DC=co,DC=uk"


# Restrict rights to edit GPO to a specific group.

# Set-GPPermissions -Name $Source_GPO_Name -PermissionLevel GpoEdit -TargetName $Access_Rights -TargetType Group
# Set-GPPermissions -Name $Source_GPO_Name -PermissionLevel GpoRead -TargetName "Authenticated Users" -TargetType Group
# Set-GPPermissions -Name $Source_GPO_Name -PermissionLevel None -TargetName "Domain Admins" -TargetType Group
# Set-GPPermissions -Name $Source_GPO_Name -PermissionLevel None -TargetName "Enterprise Admins" -TargetType Group



#'c:\\Windows\\SYSVOL\\sysvol\\' + $Domain + '\\Policies\\' + $GPO_GUID + '\\User\\Preferences\\Folders\\Folders.xml'
$New_GPO_Path = 'c:\\Windows\\SYSVOL\\sysvol\\' + $Domain + '\\Policies\\' + $GPO_GUID + '\\User'
move '.\\'+$Base_Template+'\\Preferences' $New_GPO_Path

gpupdate /force