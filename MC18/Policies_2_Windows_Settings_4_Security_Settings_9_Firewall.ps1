#			Windows Firewall with Advanced Security			# .wfw file inmport  security setting export   registry.pol parse
# Global Settings
# Policy version
		Not Configured
# Disable stateful FTP
# Disable stateful PPTP
# IPsec exempt
# IPsec through NAT
# Preshared key encoding	
# SA idle time	
# Strong CRL check	
Not Configured	
		
#		Domain Profile Settings
# Firewall state
# Inbound connections
#		Connection Security Settings



Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
{ABA4ABB3-1F48-4927-8CFC-8A83FC3F3C39}
SZ:v2.26|Action=Allow|Active=TRUE|Dir=In|Name=SSH|Edge=TRUE|






#Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName "{8A5C9C8E-5C79-4249-9305-A4F33EA4D38D}" -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=" + $Direction + "|Protocol=" + $Protocol + "|LPort=22|Name=SSH|Desc=SSH port needed for lab management. Do not override with block rules !"

#Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName "{8A5C9C8E-5C79-4249-9305-A4F33EA4D38D}" -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=In|Protocol=6|LPort=22|Name=SSH|Desc=SSH port needed for lab management. Do not override with block rules !"

#Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName "{8A5C9C8E-5C79-4249-9305-A4F33EA4D38D}" -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|LPort=67|LPort=2535|Name=DHCP|"


# Default outbound ports on windows machines.

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName "{ABA4ABB3-1F48-4927-8CFC-8A83FC3F3C39}" -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Name=SSH|Edge=TRUE|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|LPort=22|Name=SSH|Desc=SSH port needed for lab management. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|LPort=53|Name=DNS|Desc=Domain Name System. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|LPort=53|Name=DNS|Desc=Domain Name System. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|LPort=67|LPort=2535|Name=DHCP|Desc=Dynamic Host Configuration Protocol. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|LPort=67|LPort=2535|Name=DHCP|Desc=Dynamic Host Configuration Protocol. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|LPort=80|LPort=443|Name=Web Traffic|Desc=Regular web traffic ports. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|LPort=123|Name=NTP|Desc=Network Time Protocol. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|LPort=135|LPort=137|LPort=138|LPort=139|Name=NetBios|Desc=NetBIOS Name Service. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|LPort=135|LPort=137|LPort=138|LPort=139|Name=NetBios|Desc=NetBIOS Name Service. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|LPort=445|Name=SMB|Desc=Microsoft-DS (Directory Services) SMB. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|LPort=445|Name=SMB|Desc=Microsoft-DS (Directory Services) SMB. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|LPort2_10=1025-5000|LPort2_10=49152-65535|Name=RPC Ephemeral ports|Desc=Dynamically assigned outbound RPC ports. Do not override with block rules !|"

# Default inbound ports on windows machines.

Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=In|Protocol=6|LPort=22|Name=SSH|Desc=SSH port needed for lab management. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=In|Protocol=6|LPort=445|Name=SMB|Desc=Microsoft-DS (Directory Services) SMB. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=In|Protocol=17|LPort=445|Name=SMB|Desc=Microsoft-DS (Directory Services) SMB. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=In|Protocol=6|LPort=135|LPort=137|LPort=138|LPort=139|Name=NetBios|Desc=NetBIOS Name Service. Do not override with block rules !|"
Set-GPRegistryValue -Name $GPO_Name -Key "HKLM\SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules" -ValueName New-Guid -Type String -Value "v2.26|Action=Allow|Active=TRUE|Dir=In|Protocol=17|LPort=135|LPort=137|LPort=138|LPort=139|Name=NetBios|Desc=NetBIOS Name Service. Do not override with block rules !|"
















Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
{CA9CA06C-8583-4D09-9FF4-C64D8CE33D28}
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|LPort=135|App=%SystemRoot%\\system32\\svchost.exe|Name=Remote Administration (RPC-EPMAP)|Desc=Inbound rule for the RPCSS service to allow RPC/TCP traffic for all the local services.|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-NB_Name-Out-UDP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|RPort=137|App=System|Name=@FirewallAPI.dll,-32773|Desc=@FirewallAPI.dll,-32776|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-NB_Datagram-Out-UDP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|RPort=138|App=System|Name=@FirewallAPI.dll,-32781|Desc=@FirewallAPI.dll,-32784|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
FPS-NB_Session-Out-TCP
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=6|RPort=139|App=System|Name=@FirewallAPI.dll,-28507|Desc=@FirewallAPI.dll,-28510|EmbedCtxt=@FirewallAPI.dll,-28502|





Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-GP-NP-Out-TCP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|Profile=Domain|RPort=445|App=System|Name=@FirewallAPI.dll,-25401|Desc=@FirewallAPI.dll,-25401|EmbedCtxt=@FirewallAPI.dll,-25000|





Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
{0D2F93CD-CF92-410E-9FFD-F72DBF62AA4E}
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|App=C:\\Emuldata\\USER_EMULATION.exe|Name=User Emulation|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
{813BDB59-D543-4FAB-A76B-901339F39EC0}
SZ:v2.26|Action=Allow|Active=TRUE|Dir=In|App=C:\\Program Files\\OpenSSH\\sshd.exe|Name=SSHD.exe|






Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall
PolicyVersion
DWORD:538





Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
ADDS-ICMP6-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=128:*|Name=@ntdsmsg.dll,-1033|Desc=@ntdsmsg.dll,-1034|EmbedCtxt=@ntdsmsg.dll,-1026|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
ADDS-ICMP4-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=1|ICMP4=8:*|Name=@ntdsmsg.dll,-1029|Desc=@ntdsmsg.dll,-1030|EmbedCtxt=@ntdsmsg.dll,-1026|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
ADDS-UDP-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|App=%systemroot%\\System32\\lsass.exe|Name=@ntdsmsg.dll,-1008|Desc=@ntdsmsg.dll,-1021|EmbedCtxt=@ntdsmsg.dll,-1026|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
ADDS-TCP-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|App=%systemroot%\\System32\\lsass.exe|Name=@ntdsmsg.dll,-1007|Desc=@ntdsmsg.dll,-1020|EmbedCtxt=@ntdsmsg.dll,-1026|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
ADWS-TCP-Out
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=6|App=%systemroot%\\ADWS\\Microsoft.ActiveDirectory.WebServices.exe|Svc=adws|Name=@%systemroot%\\ADWS\\adwsres.dll,-7|Desc=@%systemroot%\\ADWS\\adwsres.dll,-6|EmbedCtxt=@%systemroot%\\ADWS\\adwsres.dll,-4|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-GP-LSASS-Out-TCP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|Profile=Domain|App=%SystemRoot%\\system32\\lsass.exe|Name=@FirewallAPI.dll,-25407|Desc=@FirewallAPI.dll,-25408|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-DNS-Out-UDP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|RPort=53|App=%SystemRoot%\\system32\\svchost.exe|Svc=dnscache|Name=@FirewallAPI.dll,-25405|Desc=@FirewallAPI.dll,-25406|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-GP-Out-TCP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|Profile=Domain|App=%SystemRoot%\\system32\\svchost.exe|Name=@FirewallAPI.dll,-25403|Desc=@FirewallAPI.dll,-25404|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-GP-NP-Out-TCP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|Profile=Domain|RPort=445|App=System|Name=@FirewallAPI.dll,-25401|Desc=@FirewallAPI.dll,-25401|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-IPv6-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=41|App=System|Name=@FirewallAPI.dll,-25352|Desc=@FirewallAPI.dll,-25358|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-IPHTTPS-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|RPort2_10=IPTLSOut|RPort2_10=IPHTTPSOut|App=%SystemRoot%\\system32\\svchost.exe|Svc=iphlpsvc|Name=@FirewallAPI.dll,-25427|Desc=@FirewallAPI.dll,-25429|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-Teredo-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|App=%SystemRoot%\\system32\\svchost.exe|Svc=iphlpsvc|Name=@FirewallAPI.dll,-25327|Desc=@FirewallAPI.dll,-25333|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-DHCPV6-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|LPort=546|RPort=547|App=%SystemRoot%\\system32\\svchost.exe|Svc=dhcp|Name=@FirewallAPI.dll,-25305|Desc=@FirewallAPI.dll,-25306|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-DHCP-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|LPort=68|RPort=67|App=%SystemRoot%\\system32\\svchost.exe|Svc=dhcp|Name=@FirewallAPI.dll,-25302|Desc=@FirewallAPI.dll,-25303|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-IGMP-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=2|App=System|Name=@FirewallAPI.dll,-25377|Desc=@FirewallAPI.dll,-25382|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-LD-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=132:*|RA4=LocalSubnet|RA6=LocalSubnet|Name=@FirewallAPI.dll,-25083|Desc=@FirewallAPI.dll,-25088|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-LR2-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=143:*|RA4=LocalSubnet|RA6=LocalSubnet|Name=@FirewallAPI.dll,-25076|Desc=@FirewallAPI.dll,-25081|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-LR-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=131:*|RA4=LocalSubnet|RA6=LocalSubnet|Name=@FirewallAPI.dll,-25069|Desc=@FirewallAPI.dll,-25074|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-LQ-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=130:*|RA4=LocalSubnet|RA6=LocalSubnet|Name=@FirewallAPI.dll,-25062|Desc=@FirewallAPI.dll,-25067|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-RS-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=133:*|RA4=LocalSubnet|RA6=LocalSubnet|RA6=ff02::2|RA6=fe80::/64|Name=@FirewallAPI.dll,-25008|Desc=@FirewallAPI.dll,-25011|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-RA-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=134:*|LA6=fe80::/64|RA4=LocalSubnet|RA6=LocalSubnet|RA6=ff02::1|RA6=fe80::/64|Name=@FirewallAPI.dll,-25013|Desc=@FirewallAPI.dll,-25018|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-NDA-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=136:*|Name=@FirewallAPI.dll,-25027|Desc=@FirewallAPI.dll,-25032|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-NDS-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=135:*|Name=@FirewallAPI.dll,-25020|Desc=@FirewallAPI.dll,-25025|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-PP-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=4:*|Name=@FirewallAPI.dll,-25117|Desc=@FirewallAPI.dll,-25118|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-TE-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=3:*|Name=@FirewallAPI.dll,-25114|Desc=@FirewallAPI.dll,-25115|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
CoreNet-ICMP6-PTB-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=58|ICMP6=2:*|Name=@FirewallAPI.dll,-25002|Desc=@FirewallAPI.dll,-25007|EmbedCtxt=@FirewallAPI.dll,-25000|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
Microsoft-Windows-DHCP-Failover-TCP-Out
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|RPort=647|App=%systemroot%\\system32\\svchost.exe|Svc=dhcpserver|Name=@%systemroot%\\system32\\dhcpssvc.dll,-220|Desc=@%systemroot%\\system32\\dhcpssvc.dll,-221|EmbedCtxt=@%systemroot%\\system32\\dhcpssvc.dll,-211|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
DNSSrv-UDP-Out
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=17|App=%systemroot%\\System32\\dns.exe|Svc=dns|Name=@dns.exe,-1005|Desc=@dns.exe,-1011|EmbedCtxt=@dns.exe,-1012|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
DNSSrv-TCP-Out
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=6|App=%systemroot%\\System32\\dns.exe|Svc=dns|Name=@dns.exe,-1004|Desc=@dns.exe,-1010|EmbedCtxt=@dns.exe,-1012|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
FPS-LLMNR-Out-UDP
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=17|RPort=5355|RA4=LocalSubnet|RA6=LocalSubnet|App=%SystemRoot%\\system32\\svchost.exe|Svc=dnscache|Name=@FirewallAPI.dll,-28550|Desc=@FirewallAPI.dll,-28551|EmbedCtxt=@FirewallAPI.dll,-28502|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
FPS-ICMP6-ERQ-Out
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=58|ICMP6=128:*|Name=@FirewallAPI.dll,-28546|Desc=@FirewallAPI.dll,-28547|EmbedCtxt=@FirewallAPI.dll,-28502|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
FPS-ICMP4-ERQ-Out
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=1|ICMP4=8:*|Name=@FirewallAPI.dll,-28544|Desc=@FirewallAPI.dll,-28547|EmbedCtxt=@FirewallAPI.dll,-28502|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
FPS-NB_Datagram-Out-UDP
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=17|RPort=138|App=System|Name=@FirewallAPI.dll,-28531|Desc=@FirewallAPI.dll,-28534|EmbedCtxt=@FirewallAPI.dll,-28502|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
FPS-NB_Name-Out-UDP
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=17|RPort=137|App=System|Name=@FirewallAPI.dll,-28523|Desc=@FirewallAPI.dll,-28526|EmbedCtxt=@FirewallAPI.dll,-28502|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
FPS-SMB-Out-TCP
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=6|RPort=445|App=System|Name=@FirewallAPI.dll,-28515|Desc=@FirewallAPI.dll,-28518|EmbedCtxt=@FirewallAPI.dll,-28502|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
FPS-NB_Session-Out-TCP
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=6|RPort=139|App=System|Name=@FirewallAPI.dll,-28507|Desc=@FirewallAPI.dll,-28510|EmbedCtxt=@FirewallAPI.dll,-28502|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
MDNS-Out-UDP
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=17|LPort=5353|App=%SystemRoot%\\system32\\svchost.exe|Svc=dnscache|Name=@%SystemRoot%\\system32\\firewallapi.dll,-37305|Desc=@%SystemRoot%\\system32\\firewallapi.dll,-37306|EmbedCtxt=@%SystemRoot%\\system32\\firewallapi.dll,-37302|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-FDRESPUB-WSD-Out-UDP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|RPort=3702|RA4=LocalSubnet|RA6=LocalSubnet|App=%SystemRoot%\\system32\\svchost.exe|Svc=fdrespub|Name=@FirewallAPI.dll,-32811|Desc=@FirewallAPI.dll,-32812|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-LLMNR-Out-UDP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|RPort=5355|RA4=LocalSubnet|RA6=LocalSubnet|App=%SystemRoot%\\system32\\svchost.exe|Svc=dnscache|Name=@FirewallAPI.dll,-32805|Desc=@FirewallAPI.dll,-32808|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-FDPHOST-Out-UDP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|RPort=3702|RA4=LocalSubnet|RA6=LocalSubnet|App=%SystemRoot%\\system32\\svchost.exe|Svc=fdphost|Name=@FirewallAPI.dll,-32789|Desc=@FirewallAPI.dll,-32792|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-UPnP-Out-TCP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|RPort=2869|RA4=LocalSubnet|RA6=LocalSubnet|App=%SystemRoot%\\system32\\svchost.exe|Svc=upnphost|Name=@FirewallAPI.dll,-32821|Desc=@FirewallAPI.dll,-32822|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-SSDPSrv-Out-UDP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|RPort=1900|RA4=LocalSubnet|RA6=LocalSubnet|App=%SystemRoot%\\system32\\svchost.exe|Svc=Ssdpsrv|Name=@FirewallAPI.dll,-32757|Desc=@FirewallAPI.dll,-32760|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-WSDEVNT-Out-TCP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|RPort=5357|App=System|Name=@FirewallAPI.dll,-32819|Desc=@FirewallAPI.dll,-32820|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-WSDEVNTS-Out-TCP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|RPort=5358|App=System|Name=@FirewallAPI.dll,-32815|Desc=@FirewallAPI.dll,-32816|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-NB_Datagram-Out-UDP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|RPort=138|App=System|Name=@FirewallAPI.dll,-32781|Desc=@FirewallAPI.dll,-32784|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-NB_Name-Out-UDP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=17|RPort=137|App=System|Name=@FirewallAPI.dll,-32773|Desc=@FirewallAPI.dll,-32776|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
NETDIS-UPnPHost-Out-TCP
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|RPort=2869|App=System|Name=@FirewallAPI.dll,-32765|Desc=@FirewallAPI.dll,-32768|EmbedCtxt=@FirewallAPI.dll,-32752|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
TermService-WMI-Out-TCP
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=6|App=%systemroot%\\system32\\svchost.exe|Svc=winmgmt|Name=@TSMSISrv.dll,-120|Desc=@TSMSISrv.dll,-121|EmbedCtxt=@TSMSISrv.dll,-109|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
WMI-WINMGMT-Out-TCP
SZ:v2.26|Action=Block|Active=TRUE|Dir=Out|Protocol=6|App=%SystemRoot%\\system32\\svchost.exe|Svc=winmgmt|Name=@FirewallAPI.dll,-34258|Desc=@FirewallAPI.dll,-34259|EmbedCtxt=@FirewallAPI.dll,-34251|

Computer
SOFTWARE\Policies\Microsoft\WindowsFirewall\FirewallRules
{99637E75-E270-49E7-A21C-B338678545D2}
SZ:v2.26|Action=Allow|Active=TRUE|Dir=Out|Protocol=6|LPort=11226|RPort=12311|LA4=192.168.6.0/255.255.255.0|RA4=DefaultGateway|RA6=DefaultGateway|RA4=192.168.0.1|Name=PARAMETERS RUle|
