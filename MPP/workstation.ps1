# Group policy object for windows 8.1 workstation

$UserName = ''
$Password = ''
$HostName = ''
$Domain = ''


# <RegistrySettings clsid="{A3CCFC41-DFDB-43a5-8D26-0FE8B954DA51}">

<Registry clsid="{9CD4B2F4-923D-47f5-A062-E897DD1DAD50}" name="AutoAdminLogon" status="AutoAdminLogon" image="7" changed="2018-07-09 09:58:38" uid="{C2AAA17F-EB8A-4852-B732-D411E92D9C73}"><Properties action="U" displayDecimal="0" default="0" hive="HKEY_LOCAL_MACHINE" key="SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" name="AutoAdminLogon" type="REG_SZ" value="1"/></Registry>
<Registry clsid="{9CD4B2F4-923D-47f5-A062-E897DD1DAD50}" name="DefaultDomainName" status="DefaultDomainName" image="7" changed="2018-07-09 09:59:25" uid="{F05D7E4D-AD4B-4BD1-B040-88604B5BFC9C}"><Properties action="U" displayDecimal="0" default="0" hive="HKEY_LOCAL_MACHINE" key="SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" name="DefaultDomainName" type="REG_SZ" value="NORTHWINDTRADER"/></Registry>
<Registry clsid="{9CD4B2F4-923D-47f5-A062-E897DD1DAD50}" name="DefaultPassword" status="DefaultPassword" image="7" changed="2018-07-09 10:00:00" uid="{CD569F92-45CD-4750-9AFD-61777428EF80}"><Properties action="U" displayDecimal="0" default="0" hive="HKEY_LOCAL_MACHINE" key="SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" name="DefaultPassword" type="REG_SZ" value="Student123"/></Registry>
<Registry clsid="{9CD4B2F4-923D-47f5-A062-E897DD1DAD50}" name="DefaultUserName" status="DefaultUserName" image="7" changed="2018-07-09 10:00:29" uid="{364C4AF0-CC06-4298-8DBB-58DCC0C3445F}"><Properties action="U" displayDecimal="0" default="0" hive="HKEY_LOCAL_MACHINE" key="SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" name="DefaultUserName" type="REG_SZ" value="Alice"/></Registry>
<Registry clsid="{9CD4B2F4-923D-47f5-A062-E897DD1DAD50}" name="EnableFirstLogonAnimation" status="EnableFirstLogonAnimation" image="12" changed="2018-07-09 09:57:26" uid="{749F58C4-1953-47A7-8E92-6B2A4697DF31}"><Properties action="U" displayDecimal="0" default="0" hive="HKEY_LOCAL_MACHINE" key="SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" name="EnableFirstLogonAnimation" type="REG_DWORD" value="00000000"/></Registry>
<Registry clsid="{9CD4B2F4-923D-47f5-A062-E897DD1DAD50}" name="ForceUnlockLogon" status="ForceUnlockLogon" image="12" changed="2018-07-09 09:51:26" uid="{F969FFC7-94DB-48C5-B29B-E3C339E60CCB}"><Properties action="U" displayDecimal="0" default="0" hive="HKEY_LOCAL_MACHINE" key="SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" name="ForceUnlockLogon" type="REG_DWORD" value="00000001"/></Registry>
<Registry clsid="{9CD4B2F4-923D-47f5-A062-E897DD1DAD50}" name="ShutdownWithoutLogon" status="ShutdownWithoutLogon" image="7" changed="2018-07-09 09:54:15" uid="{E500FE0A-EDF0-4012-A462-1A39270249DE}"><Properties action="U" displayDecimal="0" default="0" hive="HKEY_LOCAL_MACHINE" key="SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" name="ShutdownWithoutLogon" type="REG_SZ" value="1"/></Registry>

</RegistrySettings>
