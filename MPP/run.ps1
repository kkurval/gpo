# Northwindtraders Domain OU
New-ADOrganizationalUnit -Name "Northwindtraders" -Path "DC=northwindtraders,DC=com" -ProtectedFromAccidentalDeletion $True

# Computers OU
New-ADOrganizationalUnit -Name "Computers" -Path "OU=Northwindtraders, DC=northwindtraders,DC=com" -ProtectedFromAccidentalDeletion $True
New-ADOrganizationalUnit -Name "Domain Controllers" -Path " OU=Computers, OU=Northwindtraders, DC=northwindtraders,DC=com" -ProtectedFromAccidentalDeletion $True
New-ADOrganizationalUnit -Name "Workstations" -Path "OU=Computers, OU=Northwindtraders, DC=northwindtraders,DC=com" -ProtectedFromAccidentalDeletion $True
New-ADOrganizationalUnit -Name "Windows_8" -Path "OU=Workstations, OU=Computers, OU=Northwindtraders, DC=northwindtraders,DC=com" -ProtectedFromAccidentalDeletion $True
New-ADOrganizationalUnit -Name "Windows_10" -Path "OU=Workstations, OU=Computers, OU=Northwindtraders, DC=northwindtraders,DC=com" -ProtectedFromAccidentalDeletion $True

(New-Object System.Net.WebClient).DownloadFile('https://bitbucket.org/kkurval/gpo/raw/c4a90db4e56a9361f82235fb0db4760d5b9db161/GPOs.zip', 'GPOs.zip')
Unblock-File -Path '.\GPOs.zip'

Add-Type -AssemblyName System.IO.Compression.FileSystem

function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

Unzip ".\GPOs.zip" ".\"

Import-GPO -BackupGpoName "Northwindtraders GPO" -TargetName "Northwindtraders GPO" -path .\GPOs -CreateIfNeeded
Import-GPO -BackupGpoName "Northwindtraders GPO Windows 8" -TargetName "Northwindtraders GPO Windows 8" -path .\GPOs -CreateIfNeeded
#Import-GPO -BackupGpoName "Northwindtraders GPO Windows 10" -TargetName "Northwindtraders GPO Windows 10" -path .\GPOs -CreateIfNeeded
New-GPLink -Name "Northwindtraders GPO" -Target "dc=northwindtraders, dc=com" -Enforced Yes -LinkEnabled Yes -Order 1
New-GPLink -Name "Northwindtraders GPO Windows 8" -Target "OU=Windows_8, OU=Workstations, OU=Computers, OU=Northwindtraders, DC=northwindtraders,DC=com" -Enforced Yes -LinkEnabled Yes -Order 1
#New-GPLink -Name "Northwindtraders GPO Windows 10" -Target "OU=Windows_10, OU=Workstations, OU=Computers, OU=Northwindtraders, DC=northwindtraders,DC=com" -Enforced Yes -LinkEnabled Yes -Order 1


# Windows 10   # DESKTOP-SHUR05P
Move-ADObject -Identity 'f82a1b46-beb2-4672-ac28-7df50ef039e3' -TargetPath "OU=Windows_10, OU=Workstations, OU=Computers, OU=Northwindtraders, DC=northwindtraders, DC=com"

# Windows 8    # STUDENTPC
Move-ADObject -Identity '919d0c5b-ee33-4a3d-a938-1f5c1ceb32cd' -TargetPath "OU=Windows_8, OU=Workstations, OU=Computers, OU=Northwindtraders, DC=northwindtraders, DC=com"

# Windows 2016 # NORTH
Move-ADObject -Identity 'c718a3f6-e77a-432f-a0a1-991595591f92' -TargetPath "OU=Domain Controllers, OU=Computers, OU=Northwindtraders, DC=northwindtraders, DC=com"

New-ADGroup -Name "Lab settings" -SamAccountName LABSettings -GroupCategory Security -GroupScope Domainlocal -DisplayName "Lab Settings" -Description "DO NOT MODIFY OR REMOVE. This group is only meant for modifing lab settings."


Set-GPPermissions -Name "Northwindtraders GPO" -PermissionLevel GpoEdit -TargetName "LABSettings" -TargetType Group
Set-GPPermissions -Name "Northwindtraders GPO" -PermissionLevel GpoRead -TargetName "Authenticated Users" -TargetType Group
Set-GPPermissions -Name "Northwindtraders GPO" -PermissionLevel None -TargetName "Domain Admins" -TargetType Group
Set-GPPermissions -Name "Northwindtraders GPO" -PermissionLevel None -TargetName "Enterprise Admins" -TargetType Group

Set-GPPermissions -Name "Northwindtraders GPO Windows 8" -PermissionLevel GpoEdit -TargetName "LABSettings" -TargetType Group
Set-GPPermissions -Name "Northwindtraders GPO Windows 8" -PermissionLevel GpoRead -TargetName "Authenticated Users" -TargetType Group
Set-GPPermissions -Name "Northwindtraders GPO Windows 8" -PermissionLevel None -TargetName "Domain Admins" -TargetType Group
Set-GPPermissions -Name "Northwindtraders GPO Windows 8" -PermissionLevel None -TargetName "Enterprise Admins" -TargetType Group

#Set-GPPermissions -Name "Northwindtraders GPO Windows 10" -PermissionLevel GpoEdit -TargetName "LABSettings" -TargetType Group
#Set-GPPermissions -Name "Northwindtraders GPO Windows 10" -PermissionLevel GpoRead -TargetName "Authenticated Users" -TargetType Group
#Set-GPPermissions -Name "Northwindtraders GPO Windows 10" -PermissionLevel None -TargetName "Domain Admins" -TargetType Group
#Set-GPPermissions -Name "Northwindtraders GPO Windows 10" -PermissionLevel None -TargetName "Enterprise Admins" -TargetType Group
